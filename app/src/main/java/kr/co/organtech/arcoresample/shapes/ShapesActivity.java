package kr.co.organtech.arcoresample.shapes;

import androidx.appcompat.app.AppCompatActivity;
import kr.co.organtech.arcoresample.R;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.Material;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.BaseArFragment;

import java.util.function.Consumer;

public class ShapesActivity extends AppCompatActivity {

    private ArFragment arFragment;

    private enum ShapeType {
        CUBE,
        SPHERE,
        CYLINDER
    }

    private ShapeType shapeType = ShapeType.CUBE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shapes);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);

        Button btCube = findViewById(R.id.bt_cube);
        Button btSphere = findViewById(R.id.bt_sphere);
        Button btCylinder = findViewById(R.id.bt_cylinder);

        arFragment.setOnTapArPlaneListener(new BaseArFragment.OnTapArPlaneListener() {
            @Override
            public void onTapPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {
                if(shapeType == ShapeType.CUBE)
                    placeCube(hitResult.createAnchor());
                else if(shapeType == ShapeType.SPHERE)
                    placeSphere(hitResult.createAnchor());
                else if(shapeType == ShapeType.CYLINDER)
                    placeCylinder(hitResult.createAnchor());
            }
        });

        btCube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shapeType = ShapeType.CUBE;
            }
        });

        btSphere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shapeType = ShapeType.SPHERE;
            }
        });

        btCylinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shapeType = ShapeType.CYLINDER;
            }
        });
    }

    private void placeSphere(Anchor anchor) {
        MaterialFactory.makeOpaqueWithColor(this, new Color(android.graphics.Color.BLUE))
                .thenAccept(new Consumer<Material>() {
                    @Override
                    public void accept(Material material) {
                        ModelRenderable modelRenderable =
                                ShapeFactory.makeSphere(
                                        0.1f,
                                        new Vector3(0f, 1f, 0f),
                                        material);
                        placeModel(modelRenderable, anchor);
                    }
                });
    }

    private void placeCube(Anchor anchor) {

        MaterialFactory
                .makeOpaqueWithColor(this, new Color(android.graphics.Color.BLUE))
                .thenAccept(new Consumer<Material>() {
                    @Override
                    public void accept(Material material) {
                        ModelRenderable modelRenderable =
                                ShapeFactory.makeCube(
                                        new Vector3(0.1f, 0.1f, 0.1f),
                                        new Vector3(0f, 00.1f, 0f),
                                        material);
                        placeModel(modelRenderable, anchor);
                    }
                });

    }

    private void placeCylinder(Anchor anchor){
        MaterialFactory
                .makeOpaqueWithColor(this, new Color(android.graphics.Color.BLUE))
                .thenAccept(new Consumer<Material>() {
                    @Override
                    public void accept(Material material) {
                        ModelRenderable modelRenderable =
                                ShapeFactory.makeCylinder(
                                        0.1f,
                                        0.2f,
                                        new Vector3(0f, 0.2f, 0f),
                                        material);
                        placeModel(modelRenderable, anchor);
                    }
                });

    }

    private void placeModel(ModelRenderable modelRenderable, Anchor anchor) {

        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setRenderable(modelRenderable);
        arFragment.getArSceneView().getScene().addChild(anchorNode);
    }
}