package kr.co.organtech.arcoresample.cloud_anchors;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import kr.co.organtech.arcoresample.R;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.BaseArFragment;

public class CloudAnchorsActivity extends AppCompatActivity {

    CloudAnchorsFragment arFragment;

    private enum AppAnchorState{
        NONE,
        HOSTING,
        HOSTED
    }

    private Anchor anchor;
    private AppAnchorState appAnchorState = AppAnchorState.NONE;
    private boolean isPlaced = false;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_anchors);

        prefs = getSharedPreferences("anchorId", MODE_PRIVATE);
        editor = prefs.edit();

        arFragment = (CloudAnchorsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);

        arFragment.setOnTapArPlaneListener(new BaseArFragment.OnTapArPlaneListener() {
            @Override
            public void onTapPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {

                if(!isPlaced) {
                    anchor = arFragment.getArSceneView().getSession().hostCloudAnchor(hitResult.createAnchor());
                    appAnchorState = AppAnchorState.HOSTING;
                    showToast("Hosting...");

                    createModel(anchor);

                    isPlaced = true;
                }
            }
        });

        arFragment.getArSceneView().getScene().addOnUpdateListener(new Scene.OnUpdateListener() {
            @Override
            public void onUpdate(FrameTime frameTime) {
                if(appAnchorState != AppAnchorState.HOSTING)
                    return;

                Anchor.CloudAnchorState cloudAnchorState = anchor.getCloudAnchorState();

                if(cloudAnchorState.isError()){
                    showToast(cloudAnchorState.toString());
                }else if(cloudAnchorState == Anchor.CloudAnchorState.SUCCESS){
                    appAnchorState = AppAnchorState.HOSTED;

                    String anchorId = anchor.getCloudAnchorId();
                    editor.putString("anchorId", anchorId);
                    editor.apply();

                    showToast("Anchor hosted successfully. Anchor ID : " + anchorId);
                }
            }
        });

        Button resolve = findViewById(R.id.bt_resolve);
        resolve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String anchorId = prefs.getString("anchorId", "null");

                if(anchorId.equals("null")){
                    Toast.makeText(getApplicationContext(), "No anchorid found", Toast.LENGTH_SHORT).show();
                    return;
                }

                Anchor resolveAnchor = arFragment.getArSceneView().getSession().resolveCloudAnchor(anchorId);
                createModel(resolveAnchor);
            }
        });
    }

    private void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    private void createModel(Anchor anchor){

        ModelRenderable.builder()
                .setSource(this, R.raw.humvee)
                .build()
                .thenAccept(modelRenderable -> placeModel(anchor, modelRenderable));
    }

    private void placeModel(Anchor anchor, ModelRenderable modelRenderable) {

        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setRenderable(modelRenderable);
        arFragment.getArSceneView().getScene().addChild(anchorNode);
    }
}