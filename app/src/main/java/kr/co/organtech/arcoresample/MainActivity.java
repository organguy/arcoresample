package kr.co.organtech.arcoresample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kr.co.organtech.arcoresample.augmented_images.AugmentedImagesActivity;
import kr.co.organtech.arcoresample.cloud_anchors.CloudAnchorsActivity;
import kr.co.organtech.arcoresample.hello.HelloArActivity;
import kr.co.organtech.arcoresample.models.ModelsActivity;
import kr.co.organtech.arcoresample.shapes.ShapesActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickAR(View view) {
        switch (view.getId()){
            case  R.id.bt_hello: {

                Intent intent = new Intent(this, HelloArActivity.class);
                startActivity(intent);

                break;
            }

            case  R.id.bt_models: {

                Intent intent = new Intent(this, ModelsActivity.class);
                startActivity(intent);

                break;
            }

            case  R.id.bt_cloud_anchors: {

                Intent intent = new Intent(this, CloudAnchorsActivity.class);
                startActivity(intent);

                break;
            }
        }
    }
}