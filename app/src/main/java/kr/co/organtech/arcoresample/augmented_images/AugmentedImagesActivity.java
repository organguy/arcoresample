package kr.co.organtech.arcoresample.augmented_images;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import kr.co.organtech.arcoresample.R;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;

import com.google.ar.core.Anchor;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.rendering.ModelRenderable;

import java.util.Collection;

public class AugmentedImagesActivity extends AppCompatActivity implements Scene.OnUpdateListener {

    private AugmentedImagesFragment arFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_augmented_images);

        arFragment = (AugmentedImagesFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        arFragment.getArSceneView().getScene().addOnUpdateListener(this);
    }

    public void setupDatabase(Config config, Session session){
        Bitmap foxBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg1);
        AugmentedImageDatabase aid = new AugmentedImageDatabase(session);
        aid.addImage("fox", foxBitmap);
        config.setAugmentedImageDatabase(aid);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onUpdate(FrameTime frameTime) {

        Frame frame = arFragment.getArSceneView().getArFrame();
        Collection<AugmentedImage> images = frame.getUpdatedTrackables(AugmentedImage.class);

        for(AugmentedImage image : images){
            if(image.getTrackingState() == TrackingState.TRACKING){
                if(image.getName().equals("fox")){

                    Anchor anchor = image.createAnchor(image.getCenterPose());

                    createModel(anchor);
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void createModel(Anchor anchor) {

        ModelRenderable.builder()
                .setSource(this, R.raw.humvee)
                .build()
                .thenAccept(modelRenderable -> placeModel(modelRenderable, anchor));
    }

    private void placeModel(ModelRenderable modelRenderable, Anchor anchor) {

        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setRenderable(modelRenderable);
        arFragment.getArSceneView().getScene().addChild(anchorNode);
    }
}