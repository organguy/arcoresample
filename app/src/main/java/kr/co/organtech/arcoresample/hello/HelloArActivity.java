package kr.co.organtech.arcoresample.hello;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.assets.RenderableSource;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.AnimationData;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.BaseArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.util.function.Consumer;
import java.util.function.Function;

import kr.co.organtech.arcoresample.R;

public class HelloArActivity extends AppCompatActivity {

    ArFragment arFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_ar);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.arFragment);

        arFragment.setOnTapArPlaneListener(new BaseArFragment.OnTapArPlaneListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onTapPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {
                Anchor anchor = hitResult.createAnchor();

                String url = "android.resource://" + getPackageName() + "/" + R.raw.cat;
                Uri gltfUri = Uri.parse(url);

                ModelRenderable.builder()
                        .setSource(HelloArActivity.this, RenderableSource.builder().setSource(
                                HelloArActivity.this,
                                gltfUri,
                                RenderableSource.SourceType.GLTF2)
                                .setScale(0.75f)
                                .build())
                        .setRegistryId(url)
                        .build()
                        .thenAccept(new Consumer<ModelRenderable>() {
                            @Override
                            public void accept(ModelRenderable modelRenderable) {
                                addModelToScene(anchor, modelRenderable);
                            }
                        })
                        .exceptionally(new Function<Throwable, Void>() {
                            @Override
                            public Void apply(Throwable throwable) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(HelloArActivity.this);
                                builder.setMessage(throwable.getMessage())
                                        .show();

                                return null;
                            }
                        });

                /*ModelRenderable.builder()
                        .setSource(HelloArActivity.this, R.raw.skeleton)
                        .build()
                        .thenAccept(modelRenderable -> addModelToScene(anchor, modelRenderable))
                        .exceptionally(throwable -> {
                            AlertDialog.Builder builder = new AlertDialog.Builder(HelloArActivity.this);
                            builder.setMessage(throwable.getMessage())
                                    .show();
                            return null;
                        });*/

            }
        });
    }

    private void addModelToScene(Anchor anchor, ModelRenderable modelRenderable) {
        AnchorNode anchorNode = new AnchorNode(anchor);
        TransformableNode transformableNode = new TransformableNode(arFragment.getTransformationSystem());
        transformableNode.setParent(anchorNode);
        transformableNode.setRenderable(modelRenderable);
        arFragment.getArSceneView().getScene().addChild(anchorNode);
        transformableNode.select();
    }
}